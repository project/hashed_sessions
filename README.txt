Hashed Sessions

# Introduction

Drupal 7 core stores session ids as is. This allows anyone with read only access to the database (via SQL-i) to take a session id, present it in a session cookie to Drupal and take over the session.

This module contains a patched core session.inc that only stores hashes of the session id. Attackers cannot offer this hash as a valid session id, nor derive the session id from the hash.


# Installation

1. Put the site in maintenance mode
2. Replace the session handler in settings.php (see below)
3. Login again
4. Enable the module, existing session ids will now be hashed
5. Remove the site from maintenance mode

Users who access the site between step 3 and the end of step 4 may find themselves logged out. Step 3 can be skipped
if drush is used to enable the module.

## Replacing the session handler

To replace the session handler, point the configuration variable session_inc to the file hashed_sessions.session.inc.

As an example, one would add the following to the active settings.php:

$conf['session_inc'] = 'sites/all/modules/contrib/hashed_sessions/inc/hashed_sessions.session.inc';

# Uninstallation

1. Put the site in maintenance mode
2. Remove the $conf['session_inc'] assignment in settings.php
3. Login and disable the module
5. Remove the site from maintenance mode

All users will be logged out.

# Credits

The module uses a backport of the Drupal 8 hashed session id patch from #2164025 (https://drupal.org/node/2164025).

Original patch by Zsolt Tasnadi (https://www.drupal.org/u/skipyt) and Peter Wolanin (https://www.drupal.org/u/pwolanin).
Backport by Zsolt Tasnadi (https://www.drupal.org/u/skipyt).

Module by LimoenGroen to harden DvG installations.

# Possibilities

The change makes it possible to use the session id as a basis for a per-session encryption key provider which will be
released in the near future.
